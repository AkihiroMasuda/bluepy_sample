# coding: UTF-8
import bluepy.btle as btle
import sys
import binascii

if len(sys.argv) < 2:
    sys.exit("Usage:\n  %s <device-address>" % sys.argv[0])

# ペリフェラルへ接続
devAddr = sys.argv[1]
addrType = btle.ADDR_TYPE_RANDOM
conn = btle.Peripheral(devAddr, addrType)

# 全サービスを取得
for svc in conn.getServices():
    #サービス内のキャラクタリスティック取得
    for ch in svc.getCharacteristics():
        # 所望のUUIDに一致するものを探す
        if ch.uuid == '2c810002-2949-42e2-8368-75646ab7e134':
            # 一致するものが見つかれば、READ要求を発行
            read_val = ch.read()
            # サービスとキャラクタリスティックのUUID表示
            print("Service: uuid:{}".format(svc.uuid))
            print(" Chara.: uuid:{}".format(ch.uuid))
            # キャラクタリスティックのREAD要求結果を表示
            print("         read value > 0x{}".format(binascii.b2a_hex(read_val)) )
# 接続切断
conn.disconnect()
